import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as swStats from 'swagger-stats';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const config = new DocumentBuilder()
    .setTitle('Desafio 001')
    .setDescription('Aplicacion del deafío 01 de New Erp')
    .setVersion('1.0')
    .addTag('entidades')
    .build();

  const document = SwaggerModule.createDocument(app, config);

  SwaggerModule.setup('swagger', app, document);
  app.use(swStats.getMiddleware());

  await app.listen(3000);
}
bootstrap();
