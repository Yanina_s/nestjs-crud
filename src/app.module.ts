import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

import { TypeOrmModule } from '@nestjs/typeorm';

//Entidades
import { EntidadesModule } from './entities/entidades.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    EntidadesModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
