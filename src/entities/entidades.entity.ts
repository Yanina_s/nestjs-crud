import { Entity, Column, PrimaryGeneratedColumn} from 'typeorm'

@Entity('Entidad')
export class EntidadEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    ruc: string;
}
