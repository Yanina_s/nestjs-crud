import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

//Entidad
import { EntidadesController } from './entidades.controller';
import { EntidadesService } from './entidades.service';
import { EntidadEntity } from './entidades.entity';

@Module({
    imports: [ TypeOrmModule.forFeature([EntidadEntity]) ],
    controllers: [ EntidadesController ],
    providers: [ EntidadesService ]
})
export class EntidadesModule {}
