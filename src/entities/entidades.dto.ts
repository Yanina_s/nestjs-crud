export interface EntidadDTO {
    id: number;
    name: string;
    ruc: string;
  }