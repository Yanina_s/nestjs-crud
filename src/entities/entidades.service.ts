import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { EntidadDTO } from './entidades.dto';
import { EntidadEntity } from './entidades.entity';

@Injectable()
export class EntidadesService {
   constructor(
       @InjectRepository(EntidadEntity)
   private entityRepository: Repository<EntidadEntity>,
) {}
    
    
    async getEntities() {
        return await this.entityRepository.find();
    }

    async createEntity(data: EntidadDTO) {
        const entidad = this.entityRepository.create(data);
        await this.entityRepository.save(data);
        return entidad;
    }

    async findById(id: number):
    Promise<EntidadDTO> {
        return await this.entityRepository.findOne({
            where: {
                id: id,
            }
        });
    }

    async findByRuc(ruc: string):
    Promise<EntidadDTO> {
        return await this.entityRepository.findOne({
            where: {
                ruc: ruc,
            }
        });
    }


    async findByName(name: string):
    Promise<EntidadDTO> {
        return await this.entityRepository.findOne({
            where: {
                name: name,
            }
        });
    }

    async updateEntity(id: number, data: Partial<EntidadDTO>) {
        await this.entityRepository.update(
            {id}, 
            data
            );
        return await this.entityRepository.findOne({
            id
        });
    }

    async deleteEntity(id: number) {
       await this.entityRepository.delete({ id });
       return { deleted: true };
    }
}
