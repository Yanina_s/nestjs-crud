import { Controller, Body, Delete, Get, Param, Post, Put, HttpStatus } from '@nestjs/common';
import { EntidadesService } from './entidades.service';
import { EntidadDTO } from './entidades.dto';


@Controller('entidades')
export class EntidadesController {
    constructor(private entidadesService: EntidadesService) { }
   
    @Get()
    async getEntities() {
         return {
             statusCode: HttpStatus.OK,
             data: await this.entidadesService.getEntities(),
         };
    }


    @Get('/id/:id')
    async getById(@Param('id') id: number) {
        return {
            statusCode: HttpStatus.OK,
            data: await this.entidadesService.findById(id),
        }
    }

    @Get('/ruc/:ruc')
    async getByRuc(@Param('ruc') ruc: string) {
        return {
            statusCode: HttpStatus.OK,
            data: await this.entidadesService.findByRuc(ruc),
        }
    }

    @Get('/name/:name')
    async getByName(@Param('name') name: string) {
        return {
            statusCode: HttpStatus.OK,
            data: await this.entidadesService.findByName(name),
        }
    }

    @Post()
    async createEntity(@Body() data: EntidadDTO) {
       return {
           statusCode: HttpStatus.OK,
           message: 'Creado exitosamente',
           data: await this.entidadesService.createEntity(data),
       };
    }

    @Put('/id/:id') 
    async updateEntity(@Param('id') id: number, @Body() data: Partial<EntidadDTO>) {
        return {
            statusCode: HttpStatus.OK,
            message: 'Actualizado exitosamente',
            data: await this.entidadesService.updateEntity(id, data),
          };
    }

    @Delete('/id/:id')
    async deleteEntity(@Param('id') id: number) {
        await this.entidadesService.deleteEntity(id);
        return {
            statusCode: HttpStatus.OK,
            message: 'Eliminado exitosamente',
        };
      }
}
