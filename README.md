# Simple Nest.js CRUD

El proyecto corre en el localhost:3000

## Instalación

```bash
$ npm install
```

## Corriendo App

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

###Unit testing
```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

### API test with Postman

Métodos http a testear
- POST,
- PUT,
- GET,
- DELETE.

Rutas de las peticiones

-  POST: localhost:3000/entidades
```bash
Header: Content-Type = application/json
Body:
{
    "id": "<Un número>",
    "name": "<Un nombre>",
    "ruc": <Un RUC>
}
```

- PUT: localhost:3000/entidades/id/:id
```bash
Header: Content-Type = application/json
Body:
{
    "id": "<Un número>",
    "name": "<Un nombre>",
    "ruc": <Un RUC>
}
```

- GET: localhost:3000/entidades
- GET by ID: localhost:3000/entidades/id/:id
- GET by NAME: localhost:3000/name/:name
- GET by RUC: localhost:3000/ruc/:ruc
- DELETE: localhost:3000/entidades/id/:id

### API Documentation
```
http://<host>:3000/swagger
```
### Api Stats
```
http://<host>:3000/swagger-stats/ui
```







